# MoneyBank

This is a spring boot application of a bank called MoneyBank.

Next steps:

- [x] create project structure

- [x] add gradle

- [x] made spring boot application

- [x] add entity address and its rest controller

- [-] add some other entities

- [-] add transactions

- [-] add UI


