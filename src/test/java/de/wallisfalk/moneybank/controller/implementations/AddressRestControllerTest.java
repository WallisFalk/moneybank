package de.wallisfalk.moneybank.controller.implementations;

import de.wallisfalk.moneybank.Application;
import de.wallisfalk.moneybank.model.Address;
import de.wallisfalk.moneybank.model.AddressUpdate;
import de.wallisfalk.moneybank.model.ValidationList;
import de.wallisfalk.moneybank.utils.JsonUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Falk Wallis
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class AddressRestControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MediaType JSON = MediaType.APPLICATION_JSON;

    @Before
    public void before() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void crudTest() throws Exception {

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/count"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: count for zero elements", "0", content);

        // check invalid addresses ...
        Address invalidAddress = new Address(null, "", "", "", "");
        final String invalidAddressJson = JsonUtil.convertToString(invalidAddress);

        mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/moneybank/address/addSecure").contentType(JSON).
                accept(JSON).content(invalidAddressJson)).andExpect(MockMvcResultMatchers.status().isBadRequest()).
                andReturn();

        content = mvcResult.getResponse().getContentAsString();
        String contentWithoutPrefix = content.substring("{\"errorCodeMinusDescription\":\"400 - Bad Request\",\"message\":\"".length());
        String contentWithoutSuffix = contentWithoutPrefix.substring(0, contentWithoutPrefix.length() - 2);
        List<String> contentSplit = Arrays.asList(contentWithoutSuffix.split(";"));

        Assert.assertEquals("Case: addSecure invalid address, 1: ", Boolean.TRUE, contentSplit.contains("The " +
                "'address's field 'street' must not be null!"));
        Assert.assertEquals("Case: addSecure invalid address, 2: ", Boolean.TRUE, contentSplit.contains("The " +
                "'address's field 'homeNumber' must be at least one characters long!"));
        Assert.assertEquals("Case: addSecure invalid address, 3: ", Boolean.TRUE, contentSplit.contains("The " +
                "'address's field 'zip' must be between three and ten characters long!"));
        Assert.assertEquals("Case: addSecure invalid address, 4: ", Boolean.TRUE, contentSplit.contains("The " +
                "'address's field 'city' must be at least two characters long!"));

        // getall addresses ...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/getall"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: getall addresses (0)", "[]", content);

        // TODO: check add and addSecure respectively

        // addSecure one valid address ...
        final Address validAddress = new Address("Heinrich Zille Strasse", "1", "Hinterhaus", "01069", "Dresden");
        final String validAddressJson = JsonUtil.convertToString(validAddress);

        mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/moneybank/address/addSecure").contentType(JSON).
                accept(JSON).content(validAddressJson)).andExpect(MockMvcResultMatchers.status().isOk()).
                andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: addSecure valid address: ", validAddressJson, content);

        // get address (1) ...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/get?id=1").
                content(validAddressJson)).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: get address (1): ", validAddressJson, content);

        // get address (2) ...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/get?id=2").
                content(validAddressJson)).andExpect(MockMvcResultMatchers.status().isNotFound()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: get address (2): ", "{\"errorCodeMinusDescription\":\"404 - Not Found\"," +
                "\"message\":\"Could not find address with id = 2!\"}", content);

        // get address invalid id ...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/get?id=-1").
                content(validAddressJson)).andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: get address invalid id: ", "{\"errorCodeMinusDescription\":\"400 - Bad Request" +
                "\",\"message\":\"The 'address's field 'id' must be at least zero!\"}", content);

        // addSecure the valid address again ...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/moneybank/address/addSecure").contentType(JSON).
                accept(JSON).content(validAddressJson)).andExpect(MockMvcResultMatchers.status().isBadRequest()).
                andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: addSecure valid address again: ", "{\"errorCodeMinusDescription\":\"400 - Bad " +
                "Request\",\"message\":\"Could not add address (secure), the address 'Address{street='Heinrich Zille " +
                "Strasse', homeNumber='1', adding='Hinterhaus', zip='01069', city='Dresden'}' was added before!\"}",
                content);

        // count addresses again ...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/count"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: count for one element", "1", content);

        // getall addresses ...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/getall"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: getall addresses (1)", "[{\"street\":\"Heinrich Zille Strasse\"," +
                "\"homeNumber\":\"1\",\"adding\":\"Hinterhaus\",\"zip\":\"01069\",\"city\":\"Dresden\"}]", content);

        // exists address 1 ...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/exists?id=1"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: exists for address 1", "true", content);

        // exists address 2 ...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/exists?id=2"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: exists for address 2", "false", content);

        // exists address invalid id ...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/exists?id=-1"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: exists for address invalid id", "{\"errorCodeMinusDescription\":\"400 - Bad " +
                "Request\",\"message\":\"The 'address's field 'id' must be at least zero!\"}", content);

        // addSecure valid address two  ...
        final Address validAddressTwo = new Address("Heinrich Heine Strasse", "1", null, "01069", "Dresden");
        final String validAddressTwoJson = JsonUtil.convertToString(validAddressTwo);

        mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/moneybank/address/addSecure").contentType(JSON).
                accept(JSON).content(validAddressTwoJson)).andExpect(MockMvcResultMatchers.status().isOk()).
                andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: addSecure valid addressTwo: ", validAddressTwoJson, content);

        // add invalid address ...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/moneybank/address/add").contentType(JSON).
                accept(JSON).content(invalidAddressJson)).andExpect(MockMvcResultMatchers.status().isBadRequest()).
                andReturn();
        content = mvcResult.getResponse().getContentAsString();
        contentWithoutPrefix = content.substring("{\"errorCodeMinusDescription\":\"400 - Bad Request\",\"message\":\"".length());
        contentWithoutSuffix = contentWithoutPrefix.substring(0, contentWithoutPrefix.length() - 2);
        contentSplit = Arrays.asList(contentWithoutSuffix.split(";"));

        Assert.assertEquals("Case: add invalid address, 1: ", Boolean.TRUE, contentSplit.contains("The " +
                "'address's field 'street' must not be null!"));
        Assert.assertEquals("Case: add invalid address, 2: ", Boolean.TRUE, contentSplit.contains("The " +
                "'address's field 'homeNumber' must be at least one characters long!"));
        Assert.assertEquals("Case: add invalid address, 3: ", Boolean.TRUE, contentSplit.contains("The " +
                "'address's field 'zip' must be between three and ten characters long!"));
        Assert.assertEquals("Case: add invalid address, 4: ", Boolean.TRUE, contentSplit.contains("The " +
                "'address's field 'city' must be at least two characters long!"));


        // getEvery address (param invalid)...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/getevery?ids=1.2"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: getevery address (invalid param)", "{\"errorCodeMinusDescription\":\"400 - Bad " +
                "Request\",\"message\":\"At least one id could not be interpreted as a number!\"}", content);

        // getEvery address (param invalid 2)...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/getevery?ids=1.a"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: getevery address (invalid param 2)", "{\"errorCodeMinusDescription\":\"400 - Bad " +
                "Request\",\"message\":\"At least one id could not be interpreted as a number!\"}", content);

        // getEvery address (invalid ids)...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/getevery?ids=-1,-2"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: getevery address (invalid ids)", "{\"errorCodeMinusDescription\":\"400 - Bad " +
                "Request\",\"message\":\"The 'address's field 'id' must be at least zero!\"}", content);

        // getEvery address (no ids)...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/getevery?ids="))
                .andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: getevery address (no ids)", "{\"errorCodeMinusDescription\":\"400 - Bad Request" +
                "\",\"message\":\"The 'address's field 'commaSeparatedIds' must not be null or empty!\"}", content);

        // getEvery address (all not given) ...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/getevery?ids=3,4"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: getevery address (all not given)", "{\"errorCodeMinusDescription\":\"400 - Bad " +
                "Request\",\"message\":\"None of the given address elements could be got (due to invalid parameters " +
                "or internal errors)!\"}", content);

        // getEvery address (two pieces) ...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/getevery?ids=1,2,3,4"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: getevery address (two pieces)", "[" + validAddressJson + "," + validAddressTwoJson +
                "]", content);

        // add every address (two invalid) ...
        Address invalidAddressTwo = new Address(null, "", "", "", "");
        final String invalidAddressesJson = JsonUtil.convertToString(new ValidationList<Address>() {{
            add(invalidAddress); add(invalidAddressTwo); }});
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/moneybank/address/addevery").contentType(JSON).
                accept(JSON).content(invalidAddressesJson)).andExpect(MockMvcResultMatchers.status().isBadRequest()).
                andReturn();
        content = mvcResult.getResponse().getContentAsString();
        contentWithoutPrefix = content.substring("{\"errorCodeMinusDescription\":\"400 - Bad Request\",\"message\":\"".length());
        contentWithoutSuffix = contentWithoutPrefix.substring(0, contentWithoutPrefix.length() - 2);
        contentSplit = Arrays.asList(contentWithoutSuffix.split(";"));

        Assert.assertEquals("Case: add every address (two invalid), 1: ", Boolean.TRUE, contentSplit.contains("The " +
                "'address's field 'street' must not be null!"));
        Assert.assertEquals("Case: add every address (two invalid), 2: ", Boolean.TRUE, contentSplit.contains("The " +
                "'address's field 'homeNumber' must be at least one characters long!"));
        Assert.assertEquals("Case: add every address (two invalid), 3: ", Boolean.TRUE, contentSplit.contains("The " +
                "'address's field 'zip' must be between three and ten characters long!"));
        Assert.assertEquals("Case: add every address (two invalid), 4: ", Boolean.TRUE, contentSplit.contains("The " +
                "'address's field 'city' must be at least two characters long!"));

        // add every address (empty list) ...
        String validAddressesJson = JsonUtil.convertToString(new ValidationList<Address>());

        mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/moneybank/address/addevery").contentType(JSON).
                accept(JSON).content(validAddressesJson)).andExpect(MockMvcResultMatchers.status().isBadRequest()).
                andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: update every address (empty list)", "{\"errorCodeMinusDescription\":\"400 - Bad " +
                "Request\",\"message\":\"The Address ValidationList must not be empty!\"}", content);


        // add every address (all valid, one old, two new) ...
        final Address validAddressThree = new Address("Heinrich Böll Straße", "1", null, "01069", "Dresden");
        final Address validAddressFour = new Address("Heinrich Mann Straße", "1", "bei Schütz", "01069", "Dresden");
        validAddressesJson = JsonUtil.convertToString(new ValidationList<Address>()
        {{ add(validAddressTwo); add(validAddressThree); add(validAddressFour); }});

        mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/moneybank/address/addevery").contentType(JSON).
                accept(JSON).content(validAddressesJson)).andExpect(MockMvcResultMatchers.status().isOk()).
                andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: add every address (all valid, one old, two new)", validAddressesJson, content);

        // count addresses again ...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/count"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: count for one element", "4", content);

        // update address ...
        Address addressOneNew = validAddress;
        addressOneNew.setHomeNumber("1 a");
        String validAddressUpdateOneNewJson = JsonUtil.convertToString(new AddressUpdate(1L, addressOneNew));
        String validAddressOneNewJson = JsonUtil.convertToString(addressOneNew);
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/moneybank/address/update").contentType(JSON).
                accept(JSON).content(validAddressUpdateOneNewJson)).andExpect(MockMvcResultMatchers.status().
                isOk()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: update address one", validAddressOneNewJson, content);

        // update invalid address ...
        invalidAddress.setHomeNumber("1");
        String invalidAddressUpdateJson = JsonUtil.convertToString(new AddressUpdate(1L, invalidAddress));

        mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/moneybank/address/update").contentType(JSON).
                accept(JSON).content(invalidAddressUpdateJson)).andExpect(MockMvcResultMatchers.status().
                isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        contentWithoutPrefix = content.substring("{\"errorCodeMinusDescription\":\"400 - Bad Request\",\"message\":\"".length());
        contentWithoutSuffix = contentWithoutPrefix.substring(0, contentWithoutPrefix.length() - 2);
        contentSplit = Arrays.asList(contentWithoutSuffix.split(";"));

        Assert.assertEquals("Case: update invalid address, 1: ", Boolean.TRUE, contentSplit.contains("The " +
                "'address's field 'street' must not be null!"));
        Assert.assertEquals("Case: update invalid address, 2: ", Boolean.TRUE, contentSplit.contains("The " +
                "'address's field 'zip' must be between three and ten characters long!"));
        Assert.assertEquals("Case: update invalid address, 3: ", Boolean.TRUE, contentSplit.contains("The " +
                "'address's field 'city' must be at least two characters long!"));

        // update address (id not exists) ...
        addressOneNew.setHomeNumber("1 b");
        validAddressUpdateOneNewJson = JsonUtil.convertToString(new AddressUpdate(10L, addressOneNew));
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/moneybank/address/update").contentType(JSON).
                accept(JSON).content(validAddressUpdateOneNewJson)).andExpect(MockMvcResultMatchers.status().
                isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: update address (id not exists)", "{\"errorCodeMinusDescription\":\"400 - Bad " +
                        "Request\",\"message\":\"Could not update address, the address with id = 10 does not exists!\"}",
                content);

        // update every address (empty list) ...
        validAddressUpdateOneNewJson = JsonUtil.convertToString(new ValidationList<AddressUpdate>());
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/moneybank/address/updateevery").contentType(JSON).
                accept(JSON).content(validAddressUpdateOneNewJson)).andExpect(MockMvcResultMatchers.status().
                isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: update every address (empty list)", "{\"errorCodeMinusDescription\":\"400 - Bad " +
                "Request\",\"message\":\"The AddressUpdate ValidationList must not be empty!\"}", content);

        // update every address (only id not exists) ...
        validAddressUpdateOneNewJson = JsonUtil.convertToString(new ValidationList<AddressUpdate>() {{ add(
                new AddressUpdate(10L, addressOneNew)); }});
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/moneybank/address/updateevery").contentType(JSON).
                accept(JSON).content(validAddressUpdateOneNewJson)).andExpect(MockMvcResultMatchers.status().
                isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: update every address (only id not exists)", "{\"errorCodeMinusDescription" +
                "\":\"400 - Bad Request\",\"message\":\"None of the given address elements could be updated " +
                "(due to invalid parameters or internal errors)!\"}", content);

        // update every address (one invalid, one valid) ...
        String validAndInvalidAddressUpdateOneNewJson = JsonUtil.convertToString(new ValidationList<AddressUpdate>() {{ add(
                new AddressUpdate(10L, addressOneNew)); add(new AddressUpdate(1L, addressOneNew)); }});
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/moneybank/address/updateevery").contentType(JSON).
                accept(JSON).content(validAndInvalidAddressUpdateOneNewJson)).andExpect(MockMvcResultMatchers.status().
                isOk()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: update every address (one invalid, one valid)", JsonUtil.convertToString(
                new ArrayList<Address>() {{ add(addressOneNew); }}), content);

        // update every address (one invalid) ...
        String invalidAddressUpdateOneNewJson = JsonUtil.convertToString(new ValidationList<AddressUpdate>() {{ add(
                new AddressUpdate(1L, invalidAddress)); }});
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/moneybank/address/updateevery").contentType(JSON).
                accept(JSON).content(invalidAddressUpdateOneNewJson)).andExpect(MockMvcResultMatchers.status().
                isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        contentWithoutPrefix = content.substring("{\"errorCodeMinusDescription\":\"400 - Bad Request\",\"message\":\"".length());
        contentWithoutSuffix = contentWithoutPrefix.substring(0, contentWithoutPrefix.length() - 2);
        contentSplit = Arrays.asList(contentWithoutSuffix.split(";"));

        Assert.assertEquals("Case: update every invalid address, 1: ", Boolean.TRUE, contentSplit.contains("The " +
                "'address's field 'street' must not be null!"));
        Assert.assertEquals("Case: update every invalid address, 2: ", Boolean.TRUE, contentSplit.contains("The " +
                "'address's field 'zip' must be between three and ten characters long!"));
        Assert.assertEquals("Case: update every invalid address, 3: ", Boolean.TRUE, contentSplit.contains("The " +
                "'address's field 'city' must be at least two characters long!"));

        // remove address (no id) ...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.delete("/moneybank/address/remove?id=").
                content(validAddressJson)).andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: remove address (no id): ", "{\"errorCodeMinusDescription\":\"400 - Bad " +
                "Request\",\"message\":\"The 'address's field 'id' must not be null!\"}", content);

        // remove address (-1) ...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.delete("/moneybank/address/remove?id=-1").
                content(validAddressJson)).andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: remove address (-1): ", "{\"errorCodeMinusDescription\":\"400 - Bad Request" +
                "\",\"message\":\"The 'address's field 'id' must be at least zero!\"}", content);

        // remove address (5) ...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.delete("/moneybank/address/remove?id=5").
                content(validAddressJson)).andExpect(MockMvcResultMatchers.status().isNotFound()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: remove address (-1): ", "{\"errorCodeMinusDescription\":\"404 - Not Found" +
                "\",\"message\":\"Could not find address with id = 5!\"}", content);

        // remove address (4) ...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.delete("/moneybank/address/remove?id=4").
                content(validAddressJson)).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: remove address (-1): ", JsonUtil.convertToString(validAddressFour), content);

        // remove every address (empty list) ...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.delete("/moneybank/address/removeevery?ids=").
                content(validAddressJson)).andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: remove every address (empty list): ", "{\"errorCodeMinusDescription\":\"400 - " +
                        "Bad Request\",\"message\":\"The 'address's field 'commaSeparatedIds' must not be null or " +
                        "empty!\"}",
                content);

        // remove every address (invalid longs) ...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.delete("/moneybank/address/removeevery?ids=a,b").
                content(validAddressJson)).andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: remove every address (invalid longs): ", "{\"errorCodeMinusDescription" +
                        "\":\"400 - Bad Request\",\"message\":\"At least one id could not be interpreted as a number!\"}",
                content);

        // remove every address (invalid ids) ...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.delete("/moneybank/address/removeevery?ids=-1,-2").
                content(validAddressJson)).andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: remove every address (invalid ids): ", "{\"errorCodeMinusDescription\":\"400 - " +
                "Bad Request\",\"message\":\"The 'address's field 'id' must be at least zero!\"}", content);

        // remove every address (only not given ids) ...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.delete("/moneybank/address/removeevery?ids=10,11").
                content(validAddressJson)).andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: remove every address (only not given ids): ", "{\"errorCodeMinusDescription" +
                "\":\"400 - Bad Request\",\"message\":\"None of the given address elements could be removed (due to " +
                "invalid parameters or internal errors)!\"}", content);

        // remove every address (two given, one not given id) ...
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.delete("/moneybank/address/removeevery?ids=2,3,4").
                content(validAddressJson)).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: remove every address (two given, one not given id): ",
                JsonUtil.convertToString(new ArrayList<Address>() {{ add(validAddressTwo);
                add(validAddressThree); }}), content);

        // add every address (all valid, three new) ...
        validAddressesJson = JsonUtil.convertToString(new ValidationList<Address>()
        {{ add(validAddressTwo); add(validAddressThree); add(validAddressFour); }});

        mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/moneybank/address/addevery").contentType(JSON).
                accept(JSON).content(validAddressesJson)).andExpect(MockMvcResultMatchers.status().isOk()).
                andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: add every address (all valid, three new)", validAddressesJson, content);

        // find by street (street too short)
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/findbystreet?value=A")).andExpect(
                MockMvcResultMatchers.status().isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: find by street (street too short)", "{\"errorCodeMinusDescription" +
                "\":\"400 - Bad Request\",\"message\":\"The 'address's field 'street' must be at least two " +
                "characters long!\"}", content);

        // find by street (street Zille)
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/findbystreet?value=Heinrich " +
                "Zille Strasse")).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: find by street (street Zille)", JsonUtil.convertToString(new ArrayList<Address>()
        {{ add(addressOneNew); }}), content);

        // find by street ignore case (invalid street)
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/findbystreetignorecase?value=H")).
                andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: find by street (invalid street)", "{\"errorCodeMinusDescription\":\"400 - Bad " +
                "Request\",\"message\":\"The 'address's field 'street' must be at least two characters long!\"}",
                content);

        // find by street ignore case (street zille)
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/findbystreetignorecase?value=" +
                "HEINRICH zille Strasse")).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: find by street (street zille)", JsonUtil.convertToString(new ArrayList<Address>()
        {{ add(addressOneNew); }}), content);

        // find by home number (invalid)
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/findbyhomenumber?value=")).
                andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: find by home number (invalid)", "{\"errorCodeMinusDescription\":\"400 - Bad " +
                "Request\",\"message\":\"The 'address's field 'homeNumber' must be at least one characters long!\"}",
                content);

        // find by home number (1)
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/findbyhomenumber?value=1")).
                andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: find by home number (1)", JsonUtil.convertToString(new ArrayList<Address>()
        {{ add(validAddressTwo); add(validAddressThree); add(validAddressFour); }}), content);

        // find by home number ignore case (invalid)
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/findbyhomenumberignorecase?" +
                "value=")).andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: find by home number ignore case (invalid)", "{\"errorCodeMinusDescription" +
                "\":\"400 - Bad Request\",\"message\":\"The 'address's field 'homeNumber' must be at least one " +
                "characters long!\"}", content);

        // find by home number ignore case (1 B)
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/findbyhomenumberignorecase?" +
                "value=1 B")).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: find by home number ignore case (1 B)", JsonUtil.convertToString(
                new ArrayList<Address>() {{ add(addressOneNew); }}), content);

        // find by adding ("")
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/findbyadding?value=")).andExpect(
                MockMvcResultMatchers.status().isOk()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: find by adding ('')", JsonUtil.convertToString(new ArrayList<Address>()
        {{ add(validAddressTwo); add(validAddressThree); }}), content);

        // find by adding ignore case ("")
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/findbyaddingignorecase?value=" +
                "HINTERHAUS")).andExpect(MockMvcResultMatchers.status().isOk()).
                andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: find by adding ignore case ('')", JsonUtil.convertToString(
                new ArrayList<Address>() {{ add(addressOneNew); }}), content);

        // find by zip (invalid)
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/findbyzip?value=01")).andExpect(
                MockMvcResultMatchers.status().isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: find by zip (invalid)", "{\"errorCodeMinusDescription\":\"400 - Bad Request" +
                "\",\"message\":\"The 'address's field 'zip' must be between three and ten characters long!\"}",
                content);

        // find by zip (01069)
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/findbyzip?value=01069")).andExpect(
                MockMvcResultMatchers.status().isOk()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: find by zip (01069)", JsonUtil.convertToString(new ArrayList<Address>()
        {{ add(addressOneNew); add(validAddressTwo); add(validAddressThree); add(validAddressFour); }}), content);

        // find by city (invalid)
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/findbycity?value=D")).
                andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: find by adding (invalid)", "{\"errorCodeMinusDescription\":\"400 - Bad Request" +
                "\",\"message\":\"The 'address's field 'city' must be at least two characters long!\"}", content);

        // find by city ("Dresden")
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/findbycity?value=Dresden")).
                andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: find by adding ('')", JsonUtil.convertToString(new ArrayList<Address>()
        {{ add(addressOneNew); add(validAddressTwo); add(validAddressThree); add(validAddressFour); }}), content);

        // find by city ignore case (invalid)
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/findbycityignorecase?value=" +
                "d")).andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: find by adding (invalid)", "{\"errorCodeMinusDescription\":\"400 - Bad Request" +
                "\",\"message\":\"The 'address's field 'city' must be at least two characters long!\"}", content);

        // find by city ignore case ("dresden")
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/findbycityignorecase?value=" +
                "dresden")).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: find by adding ignore case ('')", JsonUtil.convertToString(new ArrayList<Address>()
        {{ add(addressOneNew); add(validAddressTwo); add(validAddressThree); add(validAddressFour); }}), content);

        // find by address ignore case (invalid)
        addressOneNew.setStreet(addressOneNew.getStreet().toUpperCase());
        addressOneNew.setCity(addressOneNew.getCity().toUpperCase());
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/findbyaddressignorecase").
                contentType(JSON).accept(JSON).content(JsonUtil.convertToString(invalidAddress))).andExpect(
                MockMvcResultMatchers.status().isBadRequest()).andReturn();

        content = mvcResult.getResponse().getContentAsString();
        contentWithoutPrefix = content.substring("{\"errorCodeMinusDescription\":\"400 - Bad Request\",\"message\":\"".length());
        contentWithoutSuffix = contentWithoutPrefix.substring(0, contentWithoutPrefix.length() - 2);
        contentSplit = Arrays.asList(contentWithoutSuffix.split(";"));

        Assert.assertEquals("Case: find by address (invalid), 1: ", Boolean.TRUE, contentSplit.contains("The " +
                "'address's field 'street' must not be null!"));
        Assert.assertEquals("Case: find by address (invalid), 2: ", Boolean.TRUE, contentSplit.contains("The " +
                "'address's field 'zip' must be between three and ten characters long!"));
        Assert.assertEquals("Case: find by address (invalid), 3: ", Boolean.TRUE, contentSplit.contains("The " +
                "'address's field 'city' must be at least two characters long!"));

        // find by address ignore case
        addressOneNew.setStreet(addressOneNew.getStreet().toUpperCase());
        addressOneNew.setCity(addressOneNew.getCity().toUpperCase());
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/moneybank/address/findbyaddressignorecase").
                contentType(JSON).accept(JSON).content(JsonUtil.convertToString(addressOneNew))).andExpect(
                MockMvcResultMatchers.status().isOk()).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Case: find by adding ignore case ('')", "[{\"street\":\"Heinrich Zille Strasse" +
                        "\",\"homeNumber\":\"1 b\",\"adding\":\"Hinterhaus\",\"zip\":\"01069\",\"city\":\"Dresden\"}]",
                content);
    }

}