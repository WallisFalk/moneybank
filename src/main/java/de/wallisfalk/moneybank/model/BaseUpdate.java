package de.wallisfalk.moneybank.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Falk Wallis
 *
 */
abstract class BaseUpdate {

    @NotNull(message = "The 'update's field 'id' must not be null!")
    @Min(value = 0, message = "The 'update's field 'id' must not be greater or equal than zero!")
    protected Long id;

    BaseUpdate() { }

    BaseUpdate(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
