package de.wallisfalk.moneybank.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Falk Wallis
 *
 */
@JsonPropertyOrder({ "street", "homeNumber", "adding", "zip", "city" })
public final class Address {

    @NotNull(message = "The 'address's field 'street' must not be null!")
    @Size(min = 2, message = "The 'address's field 'street' must be at least two characters long!")
    private String street;

    @NotNull(message = "The 'address's field 'homeNumber' must not be null!")
    @Size(min = 1, message = "The 'address's field 'homeNumber' must be at least one characters long!")
    private String homeNumber;

    private String adding;

    @NotNull(message = "The 'address's field 'zip' must not be null!")
    @Size(min = 3, max = 10, message = "The 'address's field 'zip' must be between three and ten characters long!")
    private String zip;

    @NotNull(message = "The 'address's field 'city' must not be null!")
    @Size(min = 2, message = "The 'address's field 'city' must be at least two characters long!")
    private String city;

    public Address() {}

    public Address(String street, String homeNumber, String adding, String zip, String city) {
        this.street = street;
        this.homeNumber = homeNumber;

        if (adding != null)
            this.adding = adding;
        else
            this.adding = "";

        this.zip = zip;
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address = (Address) o;

        if (street != null ? !street.equals(address.street) : address.street != null) return false;
        if (homeNumber != null ? !homeNumber.equals(address.homeNumber) : address.homeNumber != null) return false;
        if (adding != null ? !adding.equals(address.adding) : address.adding != null) return false;
        if (zip != null ? !zip.equals(address.zip) : address.zip != null) return false;
        return city != null ? city.equals(address.city) : address.city == null;
    }

    @Override
    public int hashCode() {
        int result = street != null ? street.hashCode() : 0;
        result = 31 * result + (homeNumber != null ? homeNumber.hashCode() : 0);
        result = 31 * result + (adding != null ? adding.hashCode() : 0);
        result = 31 * result + (zip != null ? zip.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Address{" +
                "street='" + street + '\'' +
                ", homeNumber='" + homeNumber + '\'' +
                ", adding='" + adding + '\'' +
                ", zip='" + zip + '\'' +
                ", city='" + city + '\'' +
                '}';
    }

    public String getStreet() {
        return street;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public String getAdding() {
        return adding;
    }

    public String getZip() {
        return zip;
    }

    public String getCity() {
        return city;
    }

    // ########################################

    public void setStreet(String street) {
        this.street = street;
    }

    public void setHomeNumber(String homeNumber) {
        this.homeNumber = homeNumber;
    }

    public void setAdding(String adding) {
        if (adding != null)
            this.adding = adding;
        else
            this.adding = "";
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public void setCity(String city) {
        this.city = city;
    }

}
