package de.wallisfalk.moneybank.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 *
 * @author Falk Wallis
 *
 */
@JsonPropertyOrder({ "id", "address" })
public final class AddressUpdate extends BaseUpdate {

    @Valid
    @NotNull(message = "The 'addressUpdate's field 'address' must not be null!")
    private Address address;


    public AddressUpdate() {
        super();
    }

    public AddressUpdate(Long id, Address address) {
        super(id);
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddressUpdate that = (AddressUpdate) o;
        if (id != that.id) return false;
        return Objects.equals(address, that.address);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (address != null ? address.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AddressUpdate{" +
                "id=" + id +
                ", address=" + address +
                '}';
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

}
