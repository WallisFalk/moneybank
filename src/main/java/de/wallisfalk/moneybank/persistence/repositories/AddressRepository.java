package de.wallisfalk.moneybank.persistence.repositories;

import de.wallisfalk.moneybank.persistence.entities.AddressEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 * @author Falk Wallis
 *
 */
@Repository
public interface AddressRepository extends JpaRepository<AddressEntity, Long> {

    List<AddressEntity> findByStreet(String street);

    List<AddressEntity> findByStreetIgnoreCase(String street);

    List<AddressEntity> findByHomeNumber(String homeNumber);

    List<AddressEntity> findByHomeNumberIgnoreCase(String homeNumber);

    List<AddressEntity> findByAdding(String adding);

    List<AddressEntity> findByAddingIgnoreCase(String adding);

    List<AddressEntity> findByZip(String zip);

    List<AddressEntity> findByCity(String city);

    List<AddressEntity> findByCityIgnoreCase(String city);

    List<AddressEntity> findByStreetIgnoreCaseAndHomeNumberIgnoreCaseAndAddingIgnoreCaseAndZipAndCityIgnoreCase(
            String street, String homeNumber, String adding, String zip, String city);

}
