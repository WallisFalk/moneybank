package de.wallisfalk.moneybank.persistence.entities;

import javax.persistence.*;
import java.util.Objects;

/**
 *
 * @author Falk Wallis
 *
 */
// use table name in sub classes to overwrite the applied spring.jpa.hibernate.naming strategy
@MappedSuperclass
public abstract class BaseEntity {

    @Id
    // use column name to overwrite the applied spring.jpa.hibernate.naming strategy
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    protected Long getId() {
        return id;
    }

    protected BaseEntity() { }

    protected BaseEntity(Long id) {
        this.id = id;
    }

    protected boolean idEquals(Object other) {
        if (this == other)
            return true;
        else if (other == null || getClass() != other.getClass())
            return false;
        else {
            BaseEntity otherConverted = (BaseEntity) other;
            return Objects.equals(id, otherConverted.getId());
        }
    }

}
