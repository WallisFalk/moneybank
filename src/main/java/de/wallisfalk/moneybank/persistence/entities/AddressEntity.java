package de.wallisfalk.moneybank.persistence.entities;

import de.wallisfalk.moneybank.model.Address;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author Falk Wallis
 *
 */
@Entity
@Table(name = "Address")
public class AddressEntity extends BaseEntity {

    @Column(name = "street")
    private String street;

    @Column(name = "homeNumber")
    private String homeNumber;

    @Column(name = "adding")
    private String adding;

    @Column(name = "zip")
    private String zip;

    @Column(name = "city")
    private String city;

    public AddressEntity() {}

    public AddressEntity(Address address) {
        this(null, address);
    }

    public AddressEntity(Long id, Address address) {
        super(id);
        street = address.getStreet();
        homeNumber = address.getHomeNumber();
        adding = address.getAdding();
        zip = address.getZip();
        city = address.getCity();
    }

    @Override
    public String toString() {
        return "AddressEntity{" +
                "id=" + getId() +
                ", street='" + street + '\'' +
                ", homeNumber='" + homeNumber + '\'' +
                ", adding='" + adding + '\'' +
                ", zip='" + zip + '\'' +
                ", city='" + city + '\'' +
                '}';
    }

    public String getStreet() {
        return street;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public String getAdding() {
        return adding;
    }

    public String getZip() {
        return zip;
    }

    public String getCity() {
        return city;
    }

}
