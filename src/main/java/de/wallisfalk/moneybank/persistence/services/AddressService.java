package de.wallisfalk.moneybank.persistence.services;

import de.wallisfalk.moneybank.persistence.entities.AddressEntity;
import de.wallisfalk.moneybank.persistence.repositories.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 *
 * @author Falk Wallis
 *
 */
@Component
public final class AddressService {

    @Autowired
    private AddressRepository addressRepository;

    public Long count() {
        return addressRepository.count();
    }

    public Boolean exists(Long idOfToCheck) {
        return addressRepository.existsById(idOfToCheck);
    }

    public AddressEntity findOne(Long idOfToFind) {
        return addressRepository.getOne(idOfToFind);
    }

    public List<AddressEntity> findAll() {
        return addressRepository.findAll();
    }

    public AddressEntity save(AddressEntity toSave) {
        return addressRepository.save(toSave);
    }

    public AddressEntity delete(Long idOfToDelete) {
        AddressEntity result = null;

        if (exists(idOfToDelete)) {
            AddressEntity addressEntityToDelete = findOne(idOfToDelete);

            addressRepository.delete(addressEntityToDelete);
            result = addressEntityToDelete;
        }

        return result;
    }

    public List<AddressEntity> findByStreet(String street) {
        return addressRepository.findByStreet(street);
    }

    public List<AddressEntity> findByStreetIgnoreCase(String street) {
        return addressRepository.findByStreetIgnoreCase(street);
    }

    public List<AddressEntity> findByHomeNumber(String homeNumber) {
        return addressRepository.findByHomeNumber(homeNumber);
    }

    public List<AddressEntity> findByHomeNumberIgnoreCase(String homeNumber) {
        return addressRepository.findByHomeNumberIgnoreCase(homeNumber);
    }

    public List<AddressEntity> findByAdding(String adding) {
        return addressRepository.findByAdding(adding);
    }

    public List<AddressEntity> findByAddingIgnoreCase(String adding) {
        return addressRepository.findByAddingIgnoreCase(adding);
    }

    public List<AddressEntity> findByZip(String zip) {
        return addressRepository.findByZip(zip);
    }

    public List<AddressEntity> findByCity(String city) {
        return addressRepository.findByCity(city);
    }

    public List<AddressEntity> findByCityIgnoreCase(String city) {
        return addressRepository.findByCityIgnoreCase(city);
    }

    public List<AddressEntity> findAddressesIgnoreCase(String street, String homeNumber, String adding, String zip,
                                                       String city) {
        return addressRepository.
                findByStreetIgnoreCaseAndHomeNumberIgnoreCaseAndAddingIgnoreCaseAndZipAndCityIgnoreCase(
                street, homeNumber, adding, zip, city);
    }

}

