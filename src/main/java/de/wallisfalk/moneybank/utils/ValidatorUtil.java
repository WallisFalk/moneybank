package de.wallisfalk.moneybank.utils;

import org.springframework.validation.FieldError;

import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Falk Wallis
 *
 */
public final class ValidatorUtil {

    private ValidatorUtil() { }

    public static String createEntityIdValidationErrorMessage(Long id, String entity) {
        String result = "";

        if (entity == null || entity.isEmpty())
            throw new IllegalArgumentException("The method 'ValidatorUtil.createEntityIdValidationErrorMessage's " +
                    "parameter 'entity' must not be null or empty!");

        if (id == null)
            result = "The '" + entity + "'s field 'id' must not be null!";
        else if (id < 0) {
            result = "The '" + entity + "'s field 'id' must be at least zero!";
        }

        return result;
    }

    public static String createEntityCommaSeparatedIdsValidationErrorMessage(String commaSeparatedIds, String entity) {
        String result = "";

        if (entity == null || entity.isEmpty())
            throw new IllegalArgumentException("The method 'ValidatorUtil." +
                    "createEntityCommaSeparatedIdsValidationErrorMessage's parameter 'entity' must not be null or " +
                    "empty!");

        if (commaSeparatedIds == null || commaSeparatedIds.isEmpty())
            result = "The '" + entity + "'s field 'commaSeparatedIds' must not be null or empty!";
        else if (! commaSeparatedIds.isEmpty()) {
            String commaSeparatedIdsWithoutSpaces = commaSeparatedIds.replace(" ", "");
            String[] ids = commaSeparatedIdsWithoutSpaces.split(",");

            for (String id : ids) {
                try {
                    Long.parseLong(id);
                }
                catch (NumberFormatException ex) {
                    result = "At least one id could not be interpreted as a number!";
                    break;
                }
            }
        }

        return result;
    }

    public static String extractErrorMessages(List<FieldError> fieldErrors) {
        return io.vavr.collection.List.ofAll(fieldErrors).map(FieldError::getDefaultMessage).collect(Collectors.joining(";"));
    }

}
