package de.wallisfalk.moneybank.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 *
 * @author Falk Wallis
 *
 */
public final class JsonUtil {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static String convertToString(Object toConvert) {
        try {
            return OBJECT_MAPPER.writeValueAsString(toConvert);
        }
        catch (JsonProcessingException ex) {
            // null indicates an exception
            return null;
        }
    }

    public static <T> T convertToClass(String jsonContentToConvert, Class<T> clazz) {
        try {
            return OBJECT_MAPPER.readValue(jsonContentToConvert, clazz);
        } catch (IOException ex) {
            // null indicates an exception
            return null;
        }
    }

}
