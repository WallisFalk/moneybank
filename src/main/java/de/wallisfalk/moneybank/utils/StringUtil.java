package de.wallisfalk.moneybank.utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Falk Wallis
 *
 */
public final class StringUtil {

    private StringUtil() { }

    public static List<Long> extractIdsFromCommaSeparatedString(String commaSeparatedIds) {
        List<Long> result = new ArrayList<>();

        if (commaSeparatedIds != null && ! commaSeparatedIds.isEmpty()) {
            String commaSeparatedIdsWithoutSpaces = commaSeparatedIds.replace(" ", "");
            String[] ids = commaSeparatedIdsWithoutSpaces.split(",");

            for (String id : ids) {
                try {
                    result.add(Long.parseLong(id));
                } catch (NumberFormatException ex) {
                    // do nothing
                }
            }
        }

        return result;
    }

}
