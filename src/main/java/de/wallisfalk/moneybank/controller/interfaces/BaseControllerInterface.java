package de.wallisfalk.moneybank.controller.interfaces;

import de.wallisfalk.moneybank.controller.implementations.RestControllerException;
import de.wallisfalk.moneybank.model.Address;
import de.wallisfalk.moneybank.model.AddressUpdate;
import de.wallisfalk.moneybank.model.ValidationList;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import java.util.List;

/**
 *
 * @author Falk Wallis
 *
 */
public interface BaseControllerInterface<T, TU> {

    ResponseEntity<Long> count();

    ResponseEntity<Boolean> exists(Long idOfToCheck) throws RestControllerException;

    ResponseEntity<T> get(Long idOfToGet) throws RestControllerException;

    ResponseEntity<List<T>> getEvery(String idOfToGetList) throws RestControllerException;

    ResponseEntity<List<T>> getAll();

    ResponseEntity<T> add(T toAdd, BindingResult binding) throws RestControllerException;

    ResponseEntity<T> addSecure(T toAdd, BindingResult binding) throws RestControllerException;

    ResponseEntity<List<T>> addEvery(ValidationList<Address> toAddList, BindingResult binding) throws
            RestControllerException;

    ResponseEntity<T> update(TU toUpdate, BindingResult binding) throws RestControllerException;

    ResponseEntity<List<T>> updateEvery(ValidationList<TU> toUpdateList, BindingResult binding) throws
            RestControllerException;

    ResponseEntity<T> remove(Long idOfToRemove) throws RestControllerException;

    ResponseEntity<List<T>> removeEvery(String idOfToRemoveList) throws RestControllerException;

}
