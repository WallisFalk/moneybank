package de.wallisfalk.moneybank.controller.interfaces;

import de.wallisfalk.moneybank.controller.implementations.RestControllerException;
import de.wallisfalk.moneybank.model.Address;
import de.wallisfalk.moneybank.model.AddressUpdate;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import java.util.List;

/**
 *
 * @author Falk Wallis
 *
 */
public interface AddressControllerInterface extends BaseControllerInterface<Address, AddressUpdate> {

    ResponseEntity<List<Address>> findByStreet(String street) throws RestControllerException;

    ResponseEntity<List<Address>> findByStreetIgnoreCase(String street) throws RestControllerException;

    ResponseEntity<List<Address>> findByHomeNumber(String homeNumber) throws RestControllerException;

    ResponseEntity<List<Address>> findByHomeNumberIgnoreCase(String homeNumber) throws RestControllerException;

    ResponseEntity<List<Address>> findByAdding(String adding);

    ResponseEntity<List<Address>> findByAddingIgnoreCase(String adding);

    ResponseEntity<List<Address>> findByZip(String zip) throws RestControllerException;

    ResponseEntity<List<Address>> findByCity(String city) throws RestControllerException;

    ResponseEntity<List<Address>> findByCityIgnoreCase(String city) throws RestControllerException;

    ResponseEntity<List<Address>> findAddressesIgnoreCase(Address toFind, BindingResult binding) throws RestControllerException;

}
