package de.wallisfalk.moneybank.controller.implementations;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 *
 * @author Falk Wallis
 *
 */
@JsonPropertyOrder({ "errorCodeMinusDescription", "message" })
public class RestErrorResponse {

    private String errorCodeMinusDescription;
    private String message;

    public String getErrorCodeMinusDescription() {
        return errorCodeMinusDescription;
    }

    public void setErrorCodeMinusDescription(String errorCodeMinusDescription) {
        this.errorCodeMinusDescription = errorCodeMinusDescription;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
