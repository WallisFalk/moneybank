package de.wallisfalk.moneybank.controller.implementations;

import de.wallisfalk.moneybank.controller.interfaces.AddressControllerInterface;
import de.wallisfalk.moneybank.model.Address;
import de.wallisfalk.moneybank.model.AddressUpdate;
import de.wallisfalk.moneybank.model.ValidationList;
import de.wallisfalk.moneybank.persistence.services.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static de.wallisfalk.moneybank.converter.AddressConverter.toEntity;
import static de.wallisfalk.moneybank.converter.AddressConverter.toModel;
import static de.wallisfalk.moneybank.utils.StringUtil.extractIdsFromCommaSeparatedString;
import static de.wallisfalk.moneybank.utils.ValidatorUtil.*;

/**
 *
 * @author Falk Wallis
 *
 */
@RestController
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class AddressRestController extends AddressBaseRestController implements AddressControllerInterface {

    private final String APP_JSON = "application/json";

    @Autowired
    private Validator validator;

    @Autowired
    private AddressService addressService;


    @Override
    @RequestMapping(value = "/moneybank/address/count", produces = { APP_JSON }, method = RequestMethod.GET)
    public ResponseEntity<Long> count() {
        return createResponse(Long.class, addressService.count());
    }

    @Override
    @RequestMapping(value = "/moneybank/address/exists", produces = { APP_JSON }, method = RequestMethod.GET)
    public ResponseEntity<Boolean> exists(@RequestParam(value = "id") Long idOfToCheck) throws
            RestControllerException {

        String validationErrorMessage = createEntityIdValidationErrorMessage(idOfToCheck, "address");
        if (! validationErrorMessage.isEmpty())
            return throwException(Boolean.class, validationErrorMessage);
        else
            return createResponse(Boolean.class, addressService.exists(idOfToCheck));
    }

    @Override
    @RequestMapping(value = "/moneybank/address/get", produces = { APP_JSON }, method = RequestMethod.GET)
    public ResponseEntity<Address> get(@RequestParam(value = "id") Long idOfToGet) throws RestControllerException {
        ResponseEntity<Address> result;

        String validationErrorMessage = createEntityIdValidationErrorMessage(idOfToGet, "address");
        if (! validationErrorMessage.isEmpty())
            return throwException(validationErrorMessage);
        else if (addressService.exists(idOfToGet))
            result = createResponse(toModel(addressService.findOne(idOfToGet)));
        else
            return throwException(HttpStatus.NOT_FOUND, "Could not find address with id = " +
                    idOfToGet + "!");

        return result;
    }

    @Override
    @RequestMapping(value = "/moneybank/address/getevery", produces = { APP_JSON }, method = RequestMethod.GET)
    public ResponseEntity<List<Address>> getEvery(@RequestParam(value = "ids") String idOfToGetList) throws
            RestControllerException {
        ResponseEntity<List<Address>> result = createEmptyResponseList();

        List<Long> ids;
        // could not create longs ...
        String validationErrorMessage = createEntityCommaSeparatedIdsValidationErrorMessage(idOfToGetList, "address");
        if (! validationErrorMessage.isEmpty())
            return throwExceptionList(validationErrorMessage);
        else {
            ids = extractIdsFromCommaSeparatedString(idOfToGetList);

            // check for id validity ...
            for (Long currentId : ids) {
                validationErrorMessage = createEntityIdValidationErrorMessage(currentId, "address");
                if (! validationErrorMessage.isEmpty())
                    return throwExceptionList(validationErrorMessage);
            }

            for (Long currentId : ids) {
                try {
                    result.getBody().add(get(currentId).getBody());
                } catch (RestControllerException rcEx) {
                    // do nothing
                }
            }
            if (result.getBody().size() == 0)
                return throwExceptionList("None of the given address elements could be got (due to invalid " +
                        "parameters or internal errors)!");
        }

        return result;
    }

    @Override
    @RequestMapping(value = "/moneybank/address/getall", produces = { APP_JSON }, method = RequestMethod.GET)
    public ResponseEntity<List<Address>> getAll() {
        return createResponseList(toModel(addressService.findAll()));
    }

    @Override
    @RequestMapping(value = "/moneybank/address/add", consumes = { APP_JSON }, produces = { APP_JSON },
            method = RequestMethod.PUT)
    public ResponseEntity<Address> add(@Valid @RequestBody Address toAdd, BindingResult binding)
            throws RestControllerException {
        ResponseEntity<Address> result;

        if (binding.hasFieldErrors())
            return throwException(extractErrorMessages(binding.getFieldErrors()));
        else {
            ResponseEntity<List<Address>> foundAddresses = findAddressesIgnoreCase(toAdd, createBinding(toAdd));
            if (foundAddresses.getBody().isEmpty())
                result = createResponse(toModel(addressService.save(toEntity(toAdd))));
            else
                // do not save, just return found first address
                result = createResponse(foundAddresses.getBody().get(0));
        }
        return result;
    }

    /**
     *
     * The method 'addSecure' returns in case of an existing yet address an exception.
     *
     * */
    @Override
    @RequestMapping(value = "/moneybank/address/addSecure", consumes = { APP_JSON }, produces = { APP_JSON },
            method = RequestMethod.PUT)
    public ResponseEntity<Address> addSecure(@Valid @RequestBody Address toAddSecure, BindingResult binding)
            throws RestControllerException {
        ResponseEntity<Address> result;

        if (binding.hasFieldErrors())
            return throwException(extractErrorMessages(binding.getFieldErrors()));
        else {
            ResponseEntity<List<Address>> foundAddresses = findAddressesIgnoreCase(toAddSecure, createBinding(
                    toAddSecure));
            if (foundAddresses.getBody().isEmpty())
                result = createResponse(toModel(addressService.save(toEntity(toAddSecure))));
            else
                return throwException("Could not add address (secure), the address '" + foundAddresses.getBody().
                        get(0).toString() + "' was added before!");
        }

        return result;
    }

    @Override
    @RequestMapping(value = "/moneybank/address/addevery", consumes = { APP_JSON }, produces = { APP_JSON },
            method = RequestMethod.PUT)
    public ResponseEntity<List<Address>> addEvery(@Valid @RequestBody ValidationList<Address> toAddList,
                                                  BindingResult binding) throws RestControllerException {
        ResponseEntity<List<Address>> result = createEmptyResponseList();

        if (! toAddList.isEmpty()) {
            if (binding.hasFieldErrors())
                return throwExceptionList(extractErrorMessages(binding.getFieldErrors()));
            for (Address currentAddress : toAddList) {
                result.getBody().add(add(currentAddress, createBinding(currentAddress)).getBody());
            }
        }
        else
            return throwExceptionList("The Address ValidationList must not be empty!");

        return result;
    }

    @Override
    @RequestMapping(value = "/moneybank/address/update", consumes = { APP_JSON }, produces = { APP_JSON },
            method = RequestMethod.PUT)
    public ResponseEntity<Address> update(@Valid @RequestBody AddressUpdate toUpdate, BindingResult binding) throws
            RestControllerException {
        ResponseEntity<Address> result;

        if (binding.hasFieldErrors())
            throw new RestControllerException(HttpStatus.BAD_REQUEST, extractErrorMessages(
                    binding.getFieldErrors()));
        else {
            if (exists(toUpdate.getId()).getBody())
                result = createResponse(toModel(addressService.save(toEntity(toUpdate.getId(), toUpdate.
                        getAddress()))));
            else
                return throwException("Could not update address, the address with id = " + toUpdate.getId() + " " +
                        "does not exists!");
        }

        return result;
    }

    @Override
    @RequestMapping(value = "/moneybank/address/updateevery", consumes = { APP_JSON }, produces = { APP_JSON },
            method = RequestMethod.PUT)
    public ResponseEntity<List<Address>> updateEvery(@Valid @RequestBody ValidationList<AddressUpdate> toUpdateList,
                                                     BindingResult binding) throws RestControllerException {
        ResponseEntity<List<Address>> result = createEmptyResponseList();

        if (! toUpdateList.isEmpty()) {
            if (binding.hasFieldErrors())
                return throwExceptionList(extractErrorMessages(binding.getFieldErrors()));
            for (AddressUpdate currentAddressUpdate : toUpdateList) {
                try {
                    result.getBody().add(update(currentAddressUpdate, createBinding(toUpdateList)).getBody());
                }
                catch (RestControllerException rcEx) {
                    // do nothing
                }
            }
            if (result.getBody().size() == 0)
                return throwExceptionList("None of the given address elements could be updated (due to invalid " +
                        "parameters or internal errors)!");
        }
        else
            return throwExceptionList("The AddressUpdate ValidationList must not be empty!");

        return result;
    }

    @Override
    @RequestMapping(value = "/moneybank/address/remove", produces = { APP_JSON }, method = RequestMethod.DELETE)
    public ResponseEntity<Address> remove(@RequestParam(value = "id") Long idOfToRemove) throws
            RestControllerException {
        ResponseEntity<Address> result;

        String validationErrorMessage = createEntityIdValidationErrorMessage(idOfToRemove, "address");
        if (! validationErrorMessage.isEmpty())
            return throwException(validationErrorMessage);
        else if (addressService.exists(idOfToRemove))
            result = createResponse(toModel(addressService.delete(idOfToRemove)));
        else
            return throwException(HttpStatus.NOT_FOUND, "Could not find address with id = " + idOfToRemove + "!");

        return result;
    }

    @Override
    @RequestMapping(value = "/moneybank/address/removeevery", produces = { APP_JSON }, method = RequestMethod.DELETE)
    public ResponseEntity<List<Address>> removeEvery(@RequestParam(value = "ids") String idOfToRemoveList) throws
            RestControllerException {
        ResponseEntity<List<Address>> result = createEmptyResponseList();

        List<Long> ids;
        // could not create longs ...
        String validationErrorMessage = createEntityCommaSeparatedIdsValidationErrorMessage(idOfToRemoveList,
                "address");
        if (! validationErrorMessage.isEmpty())
            return throwExceptionList(validationErrorMessage);
        else {
            ids = extractIdsFromCommaSeparatedString(idOfToRemoveList);

            // check for id validity ...
            for (Long currentId : ids) {
                validationErrorMessage = createEntityIdValidationErrorMessage(currentId, "address");
                if (! validationErrorMessage.isEmpty())
                    return throwExceptionList(validationErrorMessage);
            }

            for (Long currentId : ids) {
                try {
                    result.getBody().add(remove(currentId).getBody());
                } catch (RestControllerException rcEx) {
                    // do nothing
                }
            }
            if (result.getBody().size() == 0)
                return throwExceptionList("None of the given address elements could be removed (due to invalid " +
                        "parameters or internal errors)!");
        }

        return result;
    }



    @Override
    @RequestMapping(value = "/moneybank/address/findbystreet", produces = { APP_JSON }, method = RequestMethod.GET)
    public ResponseEntity<List<Address>> findByStreet(@RequestParam(value = "value") String street) throws
            RestControllerException {
        Address toCheck = createValidAddress();
        toCheck.setStreet(street);
        String validationErrorMessage = validateAddress(toCheck);
        if (validationErrorMessage.isEmpty())
            return createResponseList(toModel(addressService.findByStreet(street)));
        else
            return throwExceptionList(validationErrorMessage);
    }

    @Override
    @RequestMapping(value = "/moneybank/address/findbystreetignorecase", produces = { APP_JSON },
            method = RequestMethod.GET)
    public ResponseEntity<List<Address>> findByStreetIgnoreCase(@RequestParam(value = "value") String street) throws
            RestControllerException {
        Address toCheck = createValidAddress();
        toCheck.setStreet(street);
        String validationErrorMessage = validateAddress(toCheck);
        if (validationErrorMessage.isEmpty())
            return createResponseList(toModel(addressService.findByStreetIgnoreCase(street)));
        else
            return throwExceptionList(validationErrorMessage);
    }

    @Override
    @RequestMapping(value = "/moneybank/address/findbyhomenumber", produces = { APP_JSON },
            method = RequestMethod.GET)
    public ResponseEntity<List<Address>> findByHomeNumber(@RequestParam(value = "value") String homeNumber) throws
            RestControllerException {
        Address toCheck = createValidAddress();
        toCheck.setHomeNumber(homeNumber);
        String validationErrorMessage = validateAddress(toCheck);
        if (validationErrorMessage.isEmpty())
            return createResponseList(toModel(addressService.findByHomeNumber(homeNumber)));
        else
            return throwExceptionList(validationErrorMessage);
    }

    @Override
    @RequestMapping(value = "/moneybank/address/findbyhomenumberignorecase", produces = { APP_JSON },
            method = RequestMethod.GET)
    public ResponseEntity<List<Address>> findByHomeNumberIgnoreCase(@RequestParam(value = "value") String homeNumber)
            throws RestControllerException {
        Address toCheck = createValidAddress();
        toCheck.setHomeNumber(homeNumber);
        String validationErrorMessage = validateAddress(toCheck);
        if (validationErrorMessage.isEmpty())
            return createResponseList(toModel(addressService.findByHomeNumberIgnoreCase(homeNumber)));
        else
            return throwExceptionList(validationErrorMessage);
    }

    @Override
    @RequestMapping(value = "/moneybank/address/findbyadding", produces = { APP_JSON }, method = RequestMethod.GET)
    public ResponseEntity<List<Address>> findByAdding(@RequestParam(value = "value") String adding) {
        return createResponseList(toModel(addressService.findByAdding(adding)));
    }

    @Override
    @RequestMapping(value = "/moneybank/address/findbyaddingignorecase", produces = { APP_JSON },
            method = RequestMethod.GET)
    public ResponseEntity<List<Address>> findByAddingIgnoreCase(@RequestParam(value = "value") String adding) {
        return createResponseList(toModel(addressService.findByAddingIgnoreCase(adding)));
    }

    @Override
    @RequestMapping(value = "/moneybank/address/findbyzip", produces = { APP_JSON }, method = RequestMethod.GET)
    public ResponseEntity<List<Address>> findByZip(@RequestParam(value = "value") String zip) throws
            RestControllerException {
        Address toCheck = createValidAddress();
        toCheck.setZip(zip);
        String validationErrorMessage = validateAddress(toCheck);
        if (validationErrorMessage.isEmpty())
            return createResponseList(toModel(addressService.findByZip(zip)));
        else
            return throwExceptionList(validationErrorMessage);
    }

    @Override
    @RequestMapping(value = "/moneybank/address/findbycity", produces = { APP_JSON }, method = RequestMethod.GET)
    public ResponseEntity<List<Address>> findByCity(@RequestParam(value = "value") String city) throws
            RestControllerException {
        Address toCheck = createValidAddress();
        toCheck.setCity(city);
        String validationErrorMessage = validateAddress(toCheck);
        if (validationErrorMessage.isEmpty())
            return createResponseList(toModel(addressService.findByCity(city)));
        else
            return throwExceptionList(validationErrorMessage);
    }

    @Override
    @RequestMapping(value = "/moneybank/address/findbycityignorecase", produces = { APP_JSON },
            method = RequestMethod.GET)
    public ResponseEntity<List<Address>> findByCityIgnoreCase(@RequestParam(value = "value") String city) throws
            RestControllerException {
        Address toCheck = createValidAddress();
        toCheck.setCity(city);
        String validationErrorMessage = validateAddress(toCheck);
        if (validationErrorMessage.isEmpty())
            return createResponseList(toModel(addressService.findByCityIgnoreCase(city)));
        else
            return throwExceptionList(validationErrorMessage);
    }

    @Override
    @RequestMapping(value = "/moneybank/address/findbyaddressignorecase", consumes = { APP_JSON },
            produces = { APP_JSON }, method = RequestMethod.GET)
    public ResponseEntity<List<Address>> findAddressesIgnoreCase(@Valid @RequestBody Address toFind,
            BindingResult binding) throws RestControllerException {

        if (binding.hasFieldErrors())
            return throwExceptionList(extractErrorMessages(binding.getFieldErrors()));
        else
            return createResponseList(toModel(addressService.findAddressesIgnoreCase(toFind.getStreet(),
                    toFind.getHomeNumber(), toFind.getAdding(),toFind.getZip(), toFind.getCity())));
    }

    private String validateAddress(Address toCheck) {
        Errors errors = createBinding(toCheck);
        validator.validate(toCheck, errors);
        if (errors.hasFieldErrors())
            return extractErrorMessages(errors.getFieldErrors());
        else
            return "";
    }

    // this method with annotation could not be separated in e.g. abstract base class
    @ExceptionHandler(RestControllerException.class)
    public ResponseEntity<RestErrorResponse> exceptionHandler(Exception ex) {
        RestErrorResponse error = new RestErrorResponse();
        RestControllerException cEx = (RestControllerException) ex;
        error.setErrorCodeMinusDescription(Integer.toString(cEx.getHttpStatus().value()) + " - " +
                cEx.getHttpStatus().getReasonPhrase());
        error.setMessage(ex.getMessage());
        return new ResponseEntity<>(error, cEx.getHttpStatus());
    }

}
