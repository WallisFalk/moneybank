package de.wallisfalk.moneybank.controller.implementations;

import de.wallisfalk.moneybank.exceptions.ApplicationNotNestedException;
import org.springframework.http.HttpStatus;

/**
 *
 * @author Falk Wallis
 *
 */
public class RestControllerException extends ApplicationNotNestedException {
    private static final long serialVersionUID = 1L;

    private HttpStatus httpStatus;
    private String errorMessage;

    public RestControllerException(HttpStatus httpStatus, String errorMessage) {
        super(RestControllerException.class, errorMessage);

        if (httpStatus == null) {
            throw new IllegalArgumentException("The '" + this.getClass().getName() + "'s constructor parameter " +
                    "'httpStatus' must not be null!");
        }

        this.httpStatus = httpStatus;
        this.errorMessage = errorMessage;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}
