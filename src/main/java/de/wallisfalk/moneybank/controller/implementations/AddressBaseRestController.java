package de.wallisfalk.moneybank.controller.implementations;

import de.wallisfalk.moneybank.model.Address;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Falk Wallis
 *
 */
abstract class AddressBaseRestController {

    static Address createValidAddress() {
        return new Address("Ab", "1", null, "123", "Ab");
    }

    static ResponseEntity<List<Address>> createEmptyResponseList () {
        return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
    }

    static ResponseEntity<Address> createResponse(Object value) {
        return createResponse(Address.class, value);
    }

    static ResponseEntity<List<Address>> createResponseList(List<Address> values) {
        ResponseEntity<List<Address>> result = createEmptyResponseList();
        result.getBody().addAll(values);
        return result;
    }

    static <T> ResponseEntity<T> createResponse(Class<T> clazz, Object value) {
        return new ResponseEntity<>(clazz.cast(value), HttpStatus.OK);
    }

    static ResponseEntity<Address> throwException(String message) throws RestControllerException {
        return throwException(Address.class, HttpStatus.BAD_REQUEST, message);
    }

    static <T> ResponseEntity<T> throwException(Class<T> clazz, String message) throws
            RestControllerException {
        return throwException(clazz, HttpStatus.BAD_REQUEST, message);
    }

    static ResponseEntity<Address> throwException(HttpStatus status, String message) throws
            RestControllerException {
        return throwException(Address.class, status, message);
    }

    static ResponseEntity<List<Address>> throwExceptionList(String message) throws RestControllerException {
        return throwExceptionList(HttpStatus.BAD_REQUEST, message);
    }

    static <T> ResponseEntity<T> throwException(Class<T> clazz, HttpStatus status, String message) throws
            RestControllerException {
        throw new RestControllerException(status, message);
    }

    static ResponseEntity<List<Address>> throwExceptionList(HttpStatus status, String message) throws
            RestControllerException {
        throw new RestControllerException(status, message);
    }

    static BeanPropertyBindingResult createBinding(Object object) {
        return new BeanPropertyBindingResult(object, object.getClass().getName());
    }

}
