package de.wallisfalk.moneybank.exceptions;

/**
 *
 * @author Falk Wallis
 *
 */
public abstract class ApplicationRuntimeException extends RuntimeException {

    protected ApplicationRuntimeException(Class<?> clazz, String message) {
        super(message);

        if (clazz == null) {
            throw new IllegalArgumentException("The '" + this.getClass().getName() + "'s constructor parameter 'clazz' must not be null!");
        }
        checkParameterExtended(clazz, message);
    }

    protected ApplicationRuntimeException(Class<?> clazz, String message, Throwable cause) {
        super(message, cause);

        if (clazz == null) {
            throw new IllegalArgumentException("The '" + this.getClass().getName() + "'s constructor parameter 'clazz' must not be null!");
        }
        checkParameterExtended(clazz, message);
        checkParameterExtended(clazz, cause);
    }

    private void checkParameterExtended(Class<?> clazz, String message) {
        if (message == null || message.isEmpty()) {
            throw new IllegalArgumentException("The '" + clazz.getName() + "'s constructor parameter 'message' must not be null or empty!");
        }
    }

    private void checkParameterExtended(Class<?> clazz, Throwable cause) {
        if (cause == null) {
            throw new IllegalArgumentException("The '" + clazz.getName() + "'s constructor parameter 'cause' must not be null!");
        }
    }

}
