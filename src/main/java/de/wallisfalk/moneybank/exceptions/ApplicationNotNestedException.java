package de.wallisfalk.moneybank.exceptions;

/**
 *
 * @author Falk Wallis
 *
 */
public abstract class ApplicationNotNestedException extends Exception {

    protected ApplicationNotNestedException(Class<?> clazz, String message) {
        super(message);

        if (clazz == null) {
            throw new IllegalArgumentException("The '" + this.getClass().getName() + "'s constructor parameter 'clazz' must not be null!");
        }
        checkParameterExtended(clazz, message);
    }

    private void checkParameterExtended(Class<?> clazz, String message) {
        if (message == null || message.isEmpty()) {
            throw new IllegalArgumentException("The '" + clazz.getName() + "'s constructor parameter 'message' must not be null or empty!");
        }
    }

}
