package de.wallisfalk.moneybank.converter;

import de.wallisfalk.moneybank.model.Address;
import de.wallisfalk.moneybank.persistence.entities.AddressEntity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Falk Wallis
 *
 */
public final class AddressConverter {

    private AddressConverter() { }

    public static AddressEntity toEntity(Address toConvert) {
        AddressEntity result = null;

        if (toConvert != null) {
            result = new AddressEntity(toConvert);

        }

        return result;
    }

    public static AddressEntity toEntity(Long id, Address toConvert) {
        AddressEntity result = null;

        if (toConvert != null) {
            result = new AddressEntity(id, toConvert);
        }

        return result;
    }

    public static Address toModel(AddressEntity toConvert) {
        Address result = null;

        if (toConvert != null) {
            result = new Address();
            result.setStreet(toConvert.getStreet());
            result.setHomeNumber(toConvert.getHomeNumber());
            result.setAdding(toConvert.getAdding());
            result.setZip(toConvert.getZip());
            result.setCity(toConvert.getCity());
        }

        return result;
    }

    public static List<Address> toModel(List<AddressEntity> toConvertList) {
        List<Address> result = new ArrayList<>();

        if (toConvertList != null) {
            for (AddressEntity currentAddressEntity : toConvertList) {
                result.add(toModel(currentAddressEntity));
            }
        }

        return result;
    }

}
